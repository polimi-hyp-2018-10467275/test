"use strict";

var services = [
    {
        name : "Neuropsichiatria Infantile",
        locations : [
            "Istituto Gioia",
            "Centro Maria Nascente"
        ]
    },
    {
        name : "Logopedia",
        locations : [
            "Istituto Gioia",
            "Istituto Spalenza",
            "Centro Bignamini"
        ]
    },
    {
        name : "Riabilitazione Neurologica",
        locations : [
            "Istituto Castiglioni",
            "Istituto Spalenza"
        ]
    },
    {
        name : "Assistenza Domiciliare",
        locations : [
            "Istituto Gioia",
            "Istituto Castiglioni",
            "Istituto Spalenza",
            "Centro Bignamini"
        ]
    },
    {
        name : "Musicologia",
        locations : [
            "Centro Maria Nascente",
            "Centro Bignamini"
        ]
    }
    
];


var divs = $(".select-div");
var currentService = $("h1");
var serviceName = currentService[0].id;

for (var i = 0; i < services.length; i++){
        if (services[i].name == serviceName){
            for (var j = 0; j < divs.length; j++){
                if (!services[i].locations.includes(divs[j].id)){
                    divs[j].classList.add("dont-show-class");
                }
                else{
                    divs[j].classList.remove("dont-show-class");
                }
            }
        }
    }