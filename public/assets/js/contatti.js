$('.btn').on("click", function () {

    var forms = $('.needs-validation');

    Array.prototype.filter.call(forms, function (form) {

        form.addEventListener('submit', function (event) {

            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
            } else {
                form.reset();
                event.preventDefault();
                event.stopPropagation();

                var alertBox = '<div class="alert alert-success alert-dismissable">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
                    'Il tuo messaggio è stato inviato correttamente!' +
                    '</div>';

                $('#result').html(alertBox);
            }

            form.classList.add('was-validated');

        }, false);

    });

});