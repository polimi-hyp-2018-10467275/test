"use strict";

var services = [
    {
        name : "Neuropsichiatria Infantile",
        locations : [
            "Istituto Gioia",
            "Centro Maria Nascente"
        ]
    },
    {
        name : "Logopedia",
        locations : [
            "Istituto Gioia",
            "Istituto Spalenza",
            "Centro Bignamini"
        ]
    },
    {
        name : "Riabilitazione Neurologica",
        locations : [
            "Istituto Castiglioni",
            "Istituto Spalenza"
        ]
    },
    {
        name : "Assistenza Domiciliare",
        locations : [
            "Istituto Gioia",
            "Istituto Castiglioni",
            "Istituto Spalenza",
            "Centro Bignamini"
        ]
    },
    {
        name : "Musicologia",
        locations : [
            "Centro Maria Nascente",
            "Centro Bignamini"
        ]
    }
    
];
