# Hypermedia
La seguente repository contiene il progetto dell'appello di luglio del corso *Hypermedia Application* dell'anno accademico 2017-2018 tenuto presso il Politecnico di Milano dalla Professoressa Franca Garzotto.

Il progetto consiste nello sviluppo di un sito web per un care center chiamato *WeCare*, che si occupa di offrire supporto a bambini e giovani adulti affetti da disabilità e alle loro famiglie.

L'intero progetto, compresa la sua documentazione, è stato sviluppato in lingua italiana.

### Membri del gruppo ###
| Cognome e Nome  | Matricola   | Email                           |
|-----------------|:-----------:|---------------------------------|
| Molinari Alessio| 844738      | alessio1.molinari@mail.polimi.it|
| Pessina Edoardo | 905514      | edoardo2.pessina@mail.polimi.it |
| Piovani Davide  | 846034      | davide.piovani@mail.polimi.it   |
